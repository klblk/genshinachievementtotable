import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.*
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp

import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver

import com.google.api.client.util.store.FileDataStoreFactory

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets

import com.google.api.client.http.javanet.NetHttpTransport

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.json.JsonFactory
import java.io.*
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.sheets.v4.SheetsScopes

import com.google.api.services.sheets.v4.Sheets

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.services.sheets.v4.model.*

import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest


private const val APPLICATION_NAME = "Genshin Achievements Table Initer"
private val JSON_FACTORY: JsonFactory = JacksonFactory.getDefaultInstance()
private val SCOPES = listOf(SheetsScopes.SPREADSHEETS)
private const val TOKENS_DIRECTORY_PATH = "tokens"
private const val CREDENTIALS_FILE_PATH = "credentials.json"

private const val RES_FOLDER_TEXT = "GenshinData/TextMap"
private const val RES_FOLDER_DATA = "GenshinData/ExcelBinOutput"

private const val SPREADSHEET_ID_ACHIEVEMENTS = "1a78Qym1UnJGuADgSFaxXNBzYQrYvqtN_eQ5W12q005s" //my achievements
//private const val SPREADSHEET_ID_ACHIEVEMENTS = "1JvktYq92odBQsTS2tFWYsuhjk1_VOl-fTlAwIDJXc30" //shared achievements
private const val SPREADSHEET_ID_FURNITURE = "1knFfS-NF8JLOVlyXKiERI7KMOq--HobFaJ0KTZwObDk" //furniture
private const val SPREADSHEET_RANGE_ACHIEVEMENTS = "Achievements!A:G"
private const val SPREADSHEET_RANGE_FURNITURE = "Furniture!A:K"

private const val CURRENT_DATA_VERSION = "2.5.0"

fun main(args: Array<String>) {
    val service = getSheetsService()
    val ruStringMap = getStringMap("$RES_FOLDER_TEXT/TextMapRU.json")
    val enStringMap = getStringMap("$RES_FOLDER_TEXT/TextMapEN.json")
    startAchievementUpdate(service, ruStringMap, enStringMap)
//    startFurnitureUpdate(service, ruStringMap, enStringMap)
    println("Finish")
}

fun startAchievementUpdate(service: Sheets, ruStringMap: Map<Long, String>, enStringMap: Map<Long, String>) {
    println("Starting Achievements Update...")
    val oldDataMap = readOldData(service)
    val goals = getAchievementGoals()
    val achievementMap = getAchievementMap(oldDataMap)
    val tableRows = mutableListOf<List<Any>>()
    val boldRanges = mutableListOf<Pair<GridRange, Boolean>>()
    goals.forEach { goal ->
        boldRanges.add(
            GridRange()
                .setSheetId(0)
                .setStartRowIndex(tableRows.size)
                .setEndRowIndex(tableRows.size + 1)
                    to true
        )
        tableRows.add(
            listOf(
                goal.id,
                "",
                "",
                ruStringMap[goal.nameTextMapHash]!!,
                enStringMap[goal.nameTextMapHash]!!
            )
        )
        val startIndex = tableRows.size
        achievementMap[goal.id]!!.forEach { achievement ->
            val oldData = oldDataMap[achievement.id]
            val delete = if (achievement.isDeleteWatcherAfterFinish()) "???" else ""
            tableRows.add(
                listOf(
                    achievement.id,
                    oldData?.first ?: CURRENT_DATA_VERSION,
                    (oldData?.second ?: "ъ") + delete,
                    ruStringMap[achievement.titleTextMapHash]!!,
                    enStringMap[achievement.titleTextMapHash]!!,
                    ruStringMap[achievement.descTextMapHash]!!,
                    enStringMap[achievement.descTextMapHash]!!
                )
            )
        }
        boldRanges.add(
            GridRange()
                .setSheetId(0)
                .setStartRowIndex(startIndex)
                .setEndRowIndex(tableRows.size)
                    to false
        )
    }
    tableUpdate(SPREADSHEET_ID_ACHIEVEMENTS, SPREADSHEET_RANGE_ACHIEVEMENTS, service, tableRows, boldRanges)
}

fun startFurnitureUpdate(service: Sheets, ruStringMap: Map<Long, String>, enStringMap: Map<Long, String>) {
    val furnitureList = getFurniture()
    val tableRows = mutableListOf<List<Any>>()
    val boldRanges = mutableListOf<Pair<GridRange, Boolean>>()
    boldRanges.add(
        GridRange()
            .setSheetId(0)
            .setStartRowIndex(tableRows.size)
            .setEndRowIndex(tableRows.size + 1)
                to true
    )
    tableRows.add(
        listOf(
            "id",
            "game version",
            "mark",
            "name ru",
            "name en",
            "rank (★)",
            "comfort",
            "load",
            "comfort/load (bigger is better)",
            "furniture ru",
            "furniture en"
        )
    )
    val startIndex = tableRows.size
    furnitureList.forEach { furniture ->
        tableRows.add(
            listOf(
                furniture.id,
                CURRENT_DATA_VERSION,
                "",
                ruStringMap[furniture.nameTextMapHash]!!,
                enStringMap[furniture.nameTextMapHash]!!,
                "${furniture.rankLevel}★",
                furniture.comfort,
                furniture.cost,
                furniture.comfort.toFloat() / furniture.cost.toFloat(),
                ruStringMap[furniture.furnitureNameTextMapHash]!!,
                enStringMap[furniture.furnitureNameTextMapHash]!!,
            )
        )
    }
    boldRanges.add(
        GridRange()
            .setSheetId(0)
            .setStartRowIndex(startIndex)
            .setEndRowIndex(tableRows.size)
                to false
    )
    tableUpdate(SPREADSHEET_ID_FURNITURE, SPREADSHEET_RANGE_FURNITURE, service, tableRows, boldRanges)
}

private fun readOldData(service: Sheets): Map<Long, Pair<String, String>> {
    val response = service
        .spreadsheets()
        .values()
        .get(SPREADSHEET_ID_ACHIEVEMENTS, SPREADSHEET_RANGE_ACHIEVEMENTS)
        .execute()
    val values = response.getValues()
    val map = mutableMapOf<Long, Pair<String, String>>()
    values.forEach { row ->
        val id = row[0].toString()
        if (id.isNotBlank()) {
            map[id.toLong()] = row[1].toString() to row[2].toString()
        }
    }
    return map
}

private fun getStringMap(stringsFile: String): Map<Long, String> {
    val stringJson = FileReader.readFileUsingGetResource(stringsFile)
    val format = Json { }
    return format.parseToJsonElement(stringJson).jsonObject.entries
        .map {
            it.key.toLong() to it.value.jsonPrimitive.content
        }
        .toMap()
}

private fun getAchievementMap(oldDataMap: Map<Long, Pair<String, String>>): Map<Int, List<Achievement>> {
    val achievements = getAchievements()
    val achievementMap = mutableMapOf<Int, MutableList<Achievement>>()
    achievements.forEach {
        if (!it.isDeleteWatcherAfterFinish() || oldDataMap[it.id] != null) {
            val list = achievementMap.getOrPut(it.goalId, { mutableListOf() })
            list.add(it)
        }
    }
    return achievementMap
}

private fun getAchievementGoals(): List<AchievementGoal> {
    val achievementJson = FileReader.readFileUsingGetResource("$RES_FOLDER_DATA/AchievementGoalExcelConfigData.json")
    val format = Json {
        ignoreUnknownKeys = true
    }
    return format.decodeFromJsonElement(
        ListSerializer(AchievementGoal.serializer()),
        format.parseToJsonElement(achievementJson)
    )
        .sortedBy { it.orderId }
}

private fun getAchievements(): List<Achievement> {
    val achievementJson = FileReader.readFileUsingGetResource("$RES_FOLDER_DATA/AchievementExcelConfigData.json")
    val format = Json {
        ignoreUnknownKeys = true
        isLenient = true
    }
    return format.decodeFromJsonElement(
        ListSerializer(Achievement.serializer()),
        format.parseToJsonElement(achievementJson)
    )
        .sortedBy { it.orderId }
}

private fun getFurniture(): List<Furniture> {
    val furnitureJson = FileReader.readFileUsingGetResource("$RES_FOLDER_DATA/HomeWorldFurnitureExcelConfigData.json")
    val format = Json {
        ignoreUnknownKeys = true
    }
    return format.decodeFromJsonElement(
        ListSerializer(Furniture.serializer()),
        format.parseToJsonElement(furnitureJson)
    )
        .filter { it.cost != 0 && it.comfort != 0 }
        .sortedBy { it.cost.toFloat() / it.comfort.toFloat() }
}

private fun tableUpdate(
    spreadsheetId: String,
    spreadsheetRange: String,
    service: Sheets,
    tableRows: MutableList<List<Any>>,
    boldRanges: MutableList<Pair<GridRange, Boolean>>
) {
    val requests = boldRanges
        .map {
            Request()
                .setRepeatCell(
                    RepeatCellRequest()
                        .setRange(it.first)
                        .setCell(
                            CellData()
                                .setUserEnteredFormat(
                                    CellFormat()
                                        .setTextFormat(
                                            TextFormat().setBold(it.second)
                                        )
                                )
                        )
                        .setFields("*")
                )
        }
        .toMutableList()
    requests.add(
        Request()
            .setUpdateDimensionProperties(
                UpdateDimensionPropertiesRequest()
                    .setRange(
                        DimensionRange()
                            .setSheetId(0)
                            .setDimension("COLUMNS")
                            .setStartIndex(0)
                            .setEndIndex(1)
                    )
                    .setProperties(
                        DimensionProperties()
                            .setHiddenByUser(true)
                    )
                    .setFields("*")
            )
    )
    val batchBody = BatchUpdateSpreadsheetRequest()
        .setRequests(requests)
    val result = service.spreadsheets()
        .batchUpdate(spreadsheetId, batchBody)
        .execute()
    println("Cell batchUpdated ${result.replies.size}")

    val body = ValueRange().setValues(tableRows)

    val response = service
        .spreadsheets()
        .values()
        .update(spreadsheetId, spreadsheetRange, body)
        .setValueInputOption("raw")
        .execute()
    println("Cell updated ${response.updatedCells}")
}

private fun getCredentials(HTTP_TRANSPORT: NetHttpTransport): Credential {
    // Load client secrets.
    val inputStream = FileReader.readFileUsingGetResourceAsStream(CREDENTIALS_FILE_PATH)
    val clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, InputStreamReader(inputStream))

    // Build flow and trigger user authorization request.
    val flow = GoogleAuthorizationCodeFlow.Builder(
        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES
    )
        .setDataStoreFactory(FileDataStoreFactory(File(TOKENS_DIRECTORY_PATH)))
        .setAccessType("offline")
        .build()
    val receiver = LocalServerReceiver.Builder().setPort(8888).build()
    return AuthorizationCodeInstalledApp(flow, receiver).authorize("user")
}

private fun getSheetsService(): Sheets {
    val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
    return Sheets.Builder(httpTransport, JSON_FACTORY, getCredentials(httpTransport))
        .setApplicationName(APPLICATION_NAME)
        .build()
}

object FileReader {
    fun readFileUsingGetResource(fileName: String) = this::class.java.getResource(fileName).readText(Charsets.UTF_8)
    fun readFileUsingGetResourceAsStream(fileName: String): InputStream = this::class.java.getResourceAsStream(fileName)
}

@Serializable
data class AchievementGoal(
    @SerialName("Id")
    val id: Int = 0,

    @SerialName("OrderId")
    val orderId: Int,

    @SerialName("NameTextMapHash")
    val nameTextMapHash: Long
)

@Serializable
data class Achievement(
    @SerialName("GoalId")
    val goalId: Int = 0,

    @SerialName("OrderId")
    val orderId: Int = 999999,

    @SerialName("Id")
    val id: Long,

    @SerialName("TitleTextMapHash")
    val titleTextMapHash: Long,

    @SerialName("DescTextMapHash")
    val descTextMapHash: Long,

    @SerialName("IsDisuse")
    val isDisuse: Boolean = false,

    @SerialName("IsDeleteWatcherAfterFinish")
    val IsDeleteWatcherAfterFinish0: String = "false"
) {
    fun isDeleteWatcherAfterFinish() = IsDeleteWatcherAfterFinish0 == "true"
}

@Serializable
data class Furniture(
    @SerialName("Id")
    val id: Long,
    @SerialName("NameTextMapHash")
    val nameTextMapHash: Long,

    @SerialName("DescTextMapHash")
    val descTextMapHash: Long,

    @SerialName("FurnitureNameTextMapHash")
    val furnitureNameTextMapHash: Long,

    @SerialName("Comfort")
    val comfort: Int = 0,

    @SerialName("Cost")
    val cost: Int = 0,

    @SerialName("RankLevel")
    val rankLevel: Int
)